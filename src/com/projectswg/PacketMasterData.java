package com.projectswg;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import com.projectswg.packets.PacketData;

public class PacketMasterData {
	
	private final AtomicReference<LeftPanelContent> leftPanel;
	private final AtomicBoolean searchSoe;
	private final AtomicBoolean searchSwg;
	private final PacketData data;
	
	public PacketMasterData() {
		leftPanel = new AtomicReference<>(LeftPanelContent.PACKETS);
		searchSoe = new AtomicBoolean(false);
		searchSwg = new AtomicBoolean(true);
		data = new PacketData();
	}
	
	public PacketData getData() {
		return data;
	}
	
	public void setLeftPanelContent(LeftPanelContent content) {
		leftPanel.set(content);
	}
	
	public LeftPanelContent getLeftPanelContent() {
		return leftPanel.get();
	}
	
	public void setSearchSoe(boolean searchSoe) {
		this.searchSoe.set(searchSoe);
	}
	
	public boolean isSearchSoe() {
		return searchSoe.get();
	}
	
	public void setSearchSwg(boolean searchSwg) {
		this.searchSwg.set(searchSwg);
	}
	
	public boolean isSearchSwg() {
		return searchSwg.get();
	}
	
}
