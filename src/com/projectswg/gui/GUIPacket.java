package com.projectswg.gui;

import java.nio.ByteBuffer;
import java.util.Date;

import network.PacketType;
import network.packets.Packet;
import network.packets.swg.SWGPacket;

public class GUIPacket {
	
	private Packet packet;
	private PacketType type;
	private ByteBuffer data;
	private Date time;
	private int port;
	private int swgOpcode;
	private int soeOpcode;
	private boolean client;
	
	public GUIPacket() {
		this(null, null, null, 0, 0, false);
	}
	
	public GUIPacket(SWGPacket packet, PacketType type, Date time, int swgOpcode, int port, boolean client) {
		this.packet = packet;
		this.type = type;
		this.swgOpcode = swgOpcode;
		this.soeOpcode = 0;
		this.client = client;
		this.data = packet.getData();
		this.time = time;
		this.port = port;
	}
	
	public GUIPacket(Packet packet, Date time, int soeOpcode, int port, boolean client) {
		this.packet = packet;
		this.data = packet.getData();
		this.soeOpcode = soeOpcode;
		this.client = client;
		this.time = time;
		this.port = port;
		this.type = null;
		this.swgOpcode = 0;
	}
	
	public Packet getPacket() { return packet; }
	public PacketType getType() { return type; }
	public ByteBuffer getData() { return data; }
	public Date getTime() { return time; }
	public int getPort() { return port; }
	public int getSWGOpcode() { return swgOpcode; }
	public int getSOEOpcode() { return soeOpcode; }
	public boolean isClient() { return client; }
	public boolean isSWGPacket() { return swgOpcode != 0; }
	
	public String toString() {
		String packetName = getStringForSOEOpcode(soeOpcode);
		if (packet != null)
			packetName = packet.getClass().getSimpleName();
		return (client?"C -> S  ":"S -> C  ") + packetName;
	}
	
	private String getStringForSOEOpcode(int op) {
		switch (op) {
			case 1:  return "Session Request";
			case 2:  return "Session Response";
			case 3:  return "Multi-Packet";
			case 4:  return "Unused";
			case 5:  return "Disconnect";
			case 6:  return "Keep Alive";
			case 7:  return "Client Network Status Update";
			case 8:  return "Server Network Status Update";
			case 9:  return "Data Channel A";
			case 10: return "Data Channel B";
			case 11: return "Data Channel C";
			case 12: return "Data Channel D";
			case 13: return "Fragmented Data A";
			case 14: return "Fragmented Data B";
			case 15: return "Fragmented Data C";
			case 16: return "Fragmented Data D";
			case 17: return "Out of Order A";
			case 18: return "Out of Order B";
			case 19: return "Out of Order C";
			case 20: return "Out of Order D";
			case 21: return "Acknowledge A";
			case 22: return "Acknowledge B";
			case 23: return "Acknowledge C";
			case 24: return "Acknowledge D";
			case 25: return "Multi SWG A";
			case 26: return "Multi SWG B";
			case 27: return "Multi SWG C";
			case 28: return "Multi SWG D";
			case 29: return "Serious Error Acknowledge";
			case 30: return "Serious Error Reply";
		}
		return "Unknown: " + op;
	}
	
}
