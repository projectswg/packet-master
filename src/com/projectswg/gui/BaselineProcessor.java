package com.projectswg.gui;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import resources.encodables.Encodable;

import com.sun.org.apache.xerces.internal.parsers.DOMParser;

import com.projectswg.network.packets.Packet;
import network.packets.swg.zone.baselines.Baseline.BaselineType;

public class BaselineProcessor {
	
	private Map <BaselineType, BaselineObject> objects;
	
	public BaselineProcessor() {
		objects = new HashMap<>();
		loadBaselineData();
	}
	
	public Map<String, Object> process(ByteBuffer data, BaselineType type, int num) {
		Map<String, Object> variables = new LinkedHashMap<>();
		BaselineObject object = objects.get(type);
		if (object != null) {
			process(variables, object, data, num);
		}
		if (data.remaining() > 0) {
			byte[] extra = new byte[data.remaining()];
			data.get(extra);
			variables.put("EXTRA", Arrays.toString(extra));
		}
		return variables;
	}
	
	private void process(Map<String, Object> variables, BaselineObject object, ByteBuffer data, int num) {
		if (object.getParent() != null)
			process(variables, object.getParent(), data, num);
		for (BaselineVariable variable : object.getVariables()) {
			if (variable.getNum() != num)
				continue;
			try {
				variables.put(variable.getName(), processVariable(data, variable.getType()));
			} catch (Exception e) {
				System.err.println("Failed to load variable! " + variable.getNum() + "  " + variable.getName() + " / " + variable.getType());
				e.printStackTrace();
				return;
			}
		}
	}
	
	private Object processVariable(ByteBuffer data, String type) {
		switch (type) {
			case "boolean":
				return Packet.getBoolean(data);
			case "byte":
				return Packet.getByte(data);
			case "short":
				return Packet.getShort(data);
			case "int":
				return Packet.getInt(data);
			case "long":
				return Packet.getLong(data);
			case "float":
				return Packet.getFloat(data);
			case "unicode":
				return Packet.getUnicode(data);
			case "ascii":
				return Packet.getAscii(data);
			case "array":
				return Arrays.toString(Packet.getArray(data));
		}
		if (type.startsWith("SWGSet") || type.startsWith("SWGList")) { // here we go D:
			String subType = type.substring(type.indexOf(':')+1);
			byte[] extra = new byte[data.remaining()];
			int pos = data.position();
			data.get(extra);
			data.position(pos);
			int size = Packet.getInt(data);
			Packet.getInt(data); // updates
			StringBuilder bldr = new StringBuilder();
			for (int i = 0; i < size; i++) {
				if (i > 0)
					bldr.append(", ");
				bldr.append(processVariable(data, subType));
			}
			return type + "  ["+size+"] " + bldr.toString();
		} else if (type.startsWith("SWGMap")) {
			String [] subTypes = type.substring(type.indexOf(':')+1).split(",", 2);
			byte[] extra = new byte[data.remaining()];
			int pos = data.position();
			data.get(extra);
			data.position(pos);
			int size = Packet.getInt(data);
			Packet.getInt(data); // updates
			StringBuilder bldr = new StringBuilder();
			try {
				for (int i = 0; i < size; i++) {
					if (i > 0)
						bldr.append(", ");
					bldr.append(processVariable(data, subTypes[0]));
					bldr.append(processVariable(data, subTypes[1]));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return type + "  ["+size+"] " + bldr.toString();
		}
		try {
			Object o = Class.forName(type).newInstance();
			if (o instanceof Encodable) {
				Encodable e = (Encodable) o;
				e.decode(data);
				return e;
			}
			System.err.println(o.getClass().getCanonicalName() + " is not Encodable!");
		} catch (ClassNotFoundException e) {
			System.err.println(type + " has not been found!");
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return "Unknown: " + type;
	}
	
	private void loadBaselineData() {
		DOMParser baselineData = new DOMParser();
		try {
			baselineData.parse(new InputSource(new FileInputStream(new File("res/baselines.xml"))));
			Document d = baselineData.getDocument();
			NodeList children = d.getChildNodes();
			for (int i = 0; i < children.getLength(); i++) {
				Node n = children.item(i);
				if (n.getNodeName().equals("object"))
					parseObject(n, null);
			}
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
	}
	
	private void parseObject(Node n, BaselineObject parent) {
		NamedNodeMap attrs = n.getAttributes();
		BaselineObject object = new BaselineObject(parent, attrs.getNamedItem("name").getNodeValue(), attrs.getNamedItem("type").getNodeValue());
		if (object.getType() != null)
			objects.put(object.getType(), object);
		NodeList children = n.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			parseChild(object, children.item(i));
		}
	}
	
	private void parseChild(BaselineObject object, Node child) {
		if (child.getNodeName().equals("object")) {
			parseObject(child, object);
		} else if (child.getNodeName().equals("variables")) {
			NodeList childChildren = child.getChildNodes();
			int num = Integer.parseInt(child.getAttributes().getNamedItem("num").getNodeValue());
			for (int i = 0; i < childChildren.getLength(); i++) {
				child = childChildren.item(i);
				if (!child.getNodeName().equals("variable"))
					continue;
				NamedNodeMap var = child.getAttributes();
				object.addVariable(num, var.getNamedItem("name").getNodeValue(), var.getNamedItem("type").getNodeValue());
			}
		}
	}
	
	private static class BaselineObject {
		
		private final BaselineObject parent;
		private BaselineType type;
		private final List<BaselineVariable> variables;
		
		public BaselineObject(BaselineObject parent, String name, String type) {
			this.parent = parent;
			this.type = null;
			for (BaselineType baseType : BaselineType.values())
				if (baseType.toString().equals(type))
					this.type = baseType;
			this.variables = new ArrayList<>();
		}
		
		public void addVariable(int num, String name, String type) {
			variables.add(new BaselineVariable(num, name, type));
		}
		
		public BaselineObject getParent() {
			return parent;
		}
		
		public BaselineType getType() {
			return type;
		}
		
		public List<BaselineVariable> getVariables() {
			return variables;
		}
		
	}
	
	private static class BaselineVariable {
		
		private final int num;
		private final String name;
		private final String type;
		
		public BaselineVariable(int num, String name, String type) {
			this.num = num;
			this.name = name;
			this.type = type;
		}
		
		public int getNum() { return num; }
		public String getName() { return name; }
		public String getType() { return type; }
		
	}
	
}
