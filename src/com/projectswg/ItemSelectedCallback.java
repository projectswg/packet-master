package com.projectswg;

public interface ItemSelectedCallback {
	void onPacketSelected(int index);
	void onObjectSelected(long objectId);
}
