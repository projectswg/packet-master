package com.projectswg.network.packets.soe;

public interface SequencedPacket extends Comparable<SequencedPacket> {
	short getSequence();
}
