package com.projectswg;

public class UnknownPacket implements Comparable<UnknownPacket> {
	
	public int opcode;
	public int frequency;
	public String packetName;
	
	public UnknownPacket(int opcode, int frequency, String packetName) {
		this.opcode = opcode;
		this.frequency = frequency;
		this.packetName = packetName;
	}
	
	@Override
	public int compareTo(UnknownPacket packet) {
		if (packet.frequency > frequency)
			return 1;
		return packet.frequency == frequency ? 0 : -1;
	}
	
}
