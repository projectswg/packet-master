package com.projectswg.layout;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Map.Entry;

import network.packets.swg.SWGPacket;
import network.packets.swg.zone.baselines.Baseline;
import network.packets.swg.zone.deltas.DeltasMessage;

import org.jnetpcap.nio.JBuffer;

import resources.network.BaselineObject;
import resources.objects.SWGObject;
import resources.server_info.CrcDatabase;

import com.projectswg.ItemSelectedCallback;
import com.projectswg.PacketMasterData;
import com.projectswg.gui.BaselineProcessor;
import com.projectswg.gui.GUIPacket;
import com.projectswg.packets.PacketData;

import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;

public class AnalysisPanel extends Pane implements Initializable, ItemSelectedCallback {
	
	private final PacketData packetData;
	private final CrcDatabase crcDatabase;
	
	private DateFormat dateFormat;
	private GUIPacket packet;
	private TextArea packetDisplay;
	
	public AnalysisPanel(PacketMasterData data) {
		this.packetData = data.getData();
		this.crcDatabase = new CrcDatabase();
		dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS z");
		LayoutLoader.load("AnalysisPanel.fxml", this, this);
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		packetDisplay = new TextArea("");
		packetDisplay.setEditable(false);
		packetDisplay.setPadding(new Insets(5, 0, 0, 5));
		packetDisplay.prefWidthProperty().bind(widthProperty());
		packetDisplay.prefHeightProperty().bind(heightProperty());
		getChildren().add(packetDisplay);
	}
	
	@Override
	public void onPacketSelected(int index) {
		this.packet = packetData.get(index);
		StringBuilder str = new StringBuilder();
		if (packet.getSWGOpcode() != 0 && packet.getPacket() instanceof SWGPacket)
			drawSWGPacket(str);
		else
			drawSOEPacket(str);
		packetDisplay.setText(str.toString());
	}
	
	@Override
	public void onObjectSelected(long objectId) {
		StringBuilder str = new StringBuilder();
		SWGObject obj = packetData.getObject(objectId);
		if (obj != null)
			drawObject(str, obj, 0);
		packetDisplay.setText(str.toString());
	}
	
	@Override
	protected void layoutChildren() {
		super.layoutChildren();
		packetDisplay.resize(getWidth(), getHeight());
	}
	
	private void drawSWGPacket(StringBuilder str) {
		if (!(packet.getPacket() instanceof SWGPacket))
			return;
		SWGPacket packet = (SWGPacket) this.packet.getPacket();
		ByteBuffer data = packet.getData();
		data.position(0);
		drawString(str, "Date:       " + dateFormat.format(this.packet.getTime()));
		drawString(str, "Source:     " + this.packet.getPacket().getAddress() + ":" + this.packet.getPort());
		drawString(str, "Priority:   " + String.format("0x%04X", packet.getOpcode()));
		drawString(str, "SWG Opcode: " + String.format("0x%08X", packet.getSWGOpcode()).toUpperCase());
		drawString(str, "Packet:     " + packet.getClass().getSimpleName());
		if (packet instanceof Baseline) {
			Baseline b = (Baseline) packet;
			drawString(str, "Type:       " + b.getType() + " " + b.getNum());
			drawString(str, "Object ID:  " + b.getObjectId() + " [" + String.format("%016X", b.getObjectId()) + "]");
			drawStrings(str, createRaw(data));
			drawString(str, "");
			SWGObject obj = packetData.getObject(b.getObjectId());
			if (obj != null)
				drawObject(str, obj, 0);
			else
				drawString(str, "NULL");
			return;
		} else if (packet instanceof DeltasMessage) {
			DeltasMessage b = (DeltasMessage) packet;
			drawString(str, "Type:       " + b.getType() + " " + b.getNum());
			drawString(str, "Object ID:  " + b.getObjectId() + " [" + String.format("%016X", b.getObjectId()) + "]");
		}
		drawStrings(str, createRaw(data));
		drawString(str, "");
		if (packet != null)
			drawObject(str, packet, 0);
	}
	
	private List<Field> getAllFields(List<Field> fields, Class<?> type) {
		Class<?> s = type.getSuperclass();
		if (s != null && (SWGPacket.class.isAssignableFrom(s) || BaselineObject.class.isAssignableFrom(s))) {
			fields = getAllFields(fields, type.getSuperclass());
		}
		fields.addAll(Arrays.asList(type.getDeclaredFields()));
		
		return fields;
	}
	
	private void drawObject(StringBuilder str, Object o, int indentation) {
		for (Field f : getAllFields(new ArrayList<>(), o.getClass())) {
			if ((f.getModifiers() & (Modifier.STATIC | Modifier.TRANSIENT)) != 0)
				continue;
			drawObject(str, o, f, indentation);
		}
	}
	
	private void drawObject(StringBuilder str, Object o, Field f, int indentation) {
		f.setAccessible(true);
		String format = (indentation>0)?("%-"+indentation*4+"s%-25s %s"):"%s%-25s %s";
		Object field;
		try {
			field = f.get(o);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
			return;
		}
		boolean success = false;
		success = success || drawNull(str, f, field, format);
		success = success || drawNumber(str, f, field, format);
		success = success || drawEnum(str, f, field, format);
		success = success || drawCollection(str, f, field, format);
		if (!success)
			drawLeftover(str, f, field, o, format, indentation);
	}
	
	private boolean drawNull(StringBuilder str, Field f, Object field, String format) {
		if (field == null) {
			drawString(str, String.format(format, "", f.getName(), "null"));
			return true;
		}
		return false;
	}
	
	private boolean drawNumber(StringBuilder str, Field f, Object field, String format) {
		if (field instanceof Integer) {
			if (f.getName().toLowerCase(Locale.US).contains("crc")) {
				drawString(str, String.format(format+"  [%s]", "", f.getName(), field, crcDatabase.getString((Integer) field)));
			} else
				drawString(str, String.format(format, "", f.getName(), field));
		} else if (field instanceof String || field instanceof Number || field instanceof Boolean)
			drawString(str, String.format(format, "", f.getName(), field));
		else
			return false;
		return true;
	}
	
	private boolean drawEnum(StringBuilder str, Field f, Object field, String format) {
		if (field instanceof Enum<?> || field instanceof Enumeration<?>)
			drawString(str, String.format(format, "", f.getName(), field));
		else
			return false;
		return true;
	}
	
	private boolean drawCollection(StringBuilder str, Field f, Object field, String format) {
		if (field instanceof Collection)
			return drawArray(str, f, ((Collection<?>) field).toArray(), format);
		else if (field instanceof Map)
			drawString(str, String.format(format, "", f.getName(), field.getClass().getSimpleName() + "  " + field));
		else if (field.getClass().isArray())
			drawArray(str, f, field, format);
		else
			return false;
		return true;
	}
	
	private void drawLeftover(StringBuilder str, Field f, Object field, Object o, String format, int indent) {
		drawString(str, String.format(format, "", f.getName(), field.getClass().getSimpleName()));
		if (o instanceof Baseline && field instanceof byte[])
			drawBaseline(str, (Baseline) o, indent+1);
		else if (!field.getClass().isAssignableFrom(Object.class))
			drawObject(str, field, indent+1);
	}
	
	private boolean drawArray(StringBuilder str, Field f, Object field, String format) {
		String array = drawArray(field);
		if (array != null)
			drawString(str, String.format(format, "", f.getName(), f.getType().getSimpleName() + " " + array));
		else if (field != null) {
			drawString(str, String.format(format, "", f.getName(), f.getType().getName() + " ["));
			for (Object sub : (Object []) field) {
				String subStr = drawArray(sub);
				if (subStr != null)
					drawString(str, String.format("    "+format, "", "", subStr));
				else
					drawString(str, String.format("    "+format, "", "", sub));
			}
			drawString(str, String.format(format, "", "", "]"));
		} else
			return false;
		return true;
	}
	
	private String drawArray(Object o) {
		if (o instanceof byte [])
			return Arrays.toString((byte []) o);
		if (o instanceof short [])
			return Arrays.toString((short []) o);
		if (o instanceof int [])
			return Arrays.toString((int []) o);
		if (o instanceof long [])
			return Arrays.toString((long []) o);
		if (o instanceof float [])
			return Arrays.toString((float []) o);
		if (o instanceof double [])
			return Arrays.toString((double []) o);
		return null;
	}
	
	private void drawBaseline(StringBuilder str, Baseline b, int indentation) {
		String formatString = (indentation>0)?("%-"+indentation*4+"s%-25s %s"):"%s%-20s %s";
		ByteBuffer data = ByteBuffer.wrap(b.getBaselineData());
		data.getShort();
		Map <String, Object> variables = new BaselineProcessor().process(data, b.getType(), b.getNum());
		for (Entry<String, Object> e : variables.entrySet()) {
			drawString(str, String.format(formatString, "", e.getKey(), e.getValue()));
		}
	}
	
	private void drawSOEPacket(StringBuilder str) {
		ByteBuffer data = packet.getData();
		data.position(0);
		drawString(str, "Date:       " + dateFormat.format(this.packet.getTime()));
		drawString(str, "Port:       " + this.packet.getPacket().getAddress() + ":" + this.packet.getPort());
		drawString(str, "SOE Opcode: " + packet.getSOEOpcode());
		drawStrings(str, new JBuffer(data).toHexdump());
		drawString(str, "");
		drawObject(str, packet.getPacket(), 0);
	}

	private String createRaw(ByteBuffer data) {
		data.position(6);
		StringBuilder s = new StringBuilder("");
		StringBuilder ascii = new StringBuilder("");
		int ind = 0;
		int pos = 0;
		while (data.hasRemaining()) {
			if (pos > 0 && pos % 4 == 0)
				s.append(' ');
			if (pos == 16) {
				pos = 0;
				s.append("   ");
				s.append(ascii);
				ascii.delete(0, ascii.length());
				s.append('\n');
			}
			if (pos == 0) {
				s.append(String.format("%04X: ", ind));
			}
			int b = data.get() & 0xFF;
			if (b < 16)
				s.append("0" + Integer.toHexString(b).toUpperCase() + " ");
			else
				s.append(Integer.toHexString(b).toUpperCase() + " ");
			if (b >= 32 && b < 127)
				ascii.append((char)b);
			else
				ascii.append('.');
			ind++;
			pos++;
		}
		if (pos > 0) {
			for (int i = pos; i <= 16; i++) {
				if (i > 0 && i % 4 == 0)
					s.append(' ');
				if (i < 16)
					s.append("   ");
			}
			s.append("   ");
			s.append(ascii);
			ascii.delete(0, ascii.length());
		}
		return s.toString();
	}
	
	private void drawStrings(StringBuilder str, String completeStr) {
		String [] strLines = completeStr.split("\n");
		for (String s : strLines) {
			int finalIndex = 0;
			for (int i = 0; i+5 < s.length(); i++) {
				if (s.charAt(i) == ' ')
					if (s.charAt(i+1) != ' ')
						if (s.charAt(i+2) != ' ')
							if (s.charAt(i+3) == ' ')
								if (s.charAt(i+4) == ' ')
									if (s.charAt(i+5) == ' ')
										finalIndex = i+5;
			}
			s = s.substring(0, finalIndex).toUpperCase() + s.substring(finalIndex);
			drawString(str, s);
		}
	}
	
	private void drawString(StringBuilder s, String str) {
		s.append(str + '\n');
	}
	
}
