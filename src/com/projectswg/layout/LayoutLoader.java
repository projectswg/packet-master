package com.projectswg.layout;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

import javafx.fxml.FXMLLoader;

public class LayoutLoader {
	
	private static final ClassLoader CLASS_LOADER = createClassLoader();
	private static final String CLASS_PREFIX = "src/com/projectswg/layout/";
	
	private static final ClassLoader createClassLoader() {
		try {
			URL localUrl = new File("./").toURI().toURL();
			return new URLClassLoader(new URL[]{localUrl});
		} catch (MalformedURLException e) {
			return LayoutLoader.class.getClassLoader();
		}
	}
	
	public static <T> T load(String name) {
		return load(name, null, null);
	}
	
	public static <T> T load(String name, Object controller) {
		return load(name, null, controller);
	}
	
	public static <T> T load(String name, Object root, Object controller) {
		FXMLLoader loader = new FXMLLoader(CLASS_LOADER.getResource(CLASS_PREFIX + name));
		if (root != null)
			loader.setRoot(root);
		if (controller != null)
			loader.setController(controller);
		try {
			return loader.load();
		} catch (IOException e) {
			return null;
		}
	}
	
	public static URL getResource(String name) {
		return CLASS_LOADER.getResource(name);
	}
	
}
