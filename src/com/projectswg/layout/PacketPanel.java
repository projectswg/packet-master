package com.projectswg.layout;

import java.net.URL;
import java.util.ResourceBundle;

import resources.objects.SWGObject;

import com.projectswg.ItemSelectedCallback;
import com.projectswg.LeftPanelContent;
import com.projectswg.PacketMasterData;
import com.projectswg.gui.GUIPacket;
import com.projectswg.packets.PacketData;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

public class PacketPanel extends Pane implements Initializable, ItemSelectedCallback {
	
	private final PacketMasterData packetMasterData;
	private final PacketData packetData;
	
	private ListView<GUIPacket> packetList;
	private ListView<SWGObject> objectList;
	private ObservableList<GUIPacket> packetItems;
	private ObservableList<SWGObject> objectItems;
	private ItemSelectedCallback selectedCallback;
	private TabPane tabPane;
	private Tab packetTab;
	private Tab objectTab;
	
	public PacketPanel(PacketMasterData packetData) {
		this.packetMasterData = packetData;
		this.packetData = packetData.getData();
		LayoutLoader.load("PacketPanel.fxml", this, this);
		selectedCallback = null;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		packetList = new ListView<>();
		packetList.setEditable(false);
		packetList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		packetList.setCellFactory((param) -> new PacketListCell());
		packetList.setOnMouseClicked((event) -> {
			if (selectedCallback != null) {
				int index = packetList.getSelectionModel().getSelectedIndex();
				if (index >= 0 && index < packetData.size()) {
					packetData.setSelectedIndex(index);
					selectedCallback.onPacketSelected(index);
				}
			}
		});
		objectList = new ListView<>();
		objectList.setEditable(false);
		objectList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		objectList.setCellFactory((param) -> new ObjectListCell());
		objectList.setOnMouseClicked((event) -> {
			if (selectedCallback != null) {
				SWGObject obj = objectList.getSelectionModel().getSelectedItem();
				if (obj == null)
					return;
				long id = obj.getObjectId();
				packetData.setSelectedId(id);
				selectedCallback.onObjectSelected(id);
			}
		});
		updatePackets();
		updateObjects();
		tabPane = new TabPane();
		packetTab = new Tab("Packets", packetList);
		objectTab = new Tab("Objects", objectList);
		tabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
			@Override
			public void changed(ObservableValue<? extends Tab> ov, Tab oldTab, Tab newTab) {
				if (newTab == packetTab)
					packetMasterData.setLeftPanelContent(LeftPanelContent.PACKETS);
				else if (newTab == objectTab)
					packetMasterData.setLeftPanelContent(LeftPanelContent.OBJECTS);
			}
		});
		packetTab.setClosable(false);
		objectTab.setClosable(false);
		packetTab.setTooltip(new Tooltip("Raw Packet List"));
		objectTab.setTooltip(new Tooltip("Object List"));
		tabPane.getTabs().add(packetTab);
		tabPane.getTabs().add(objectTab);
		getChildren().add(tabPane);
	}
	
	@Override
	public void onPacketSelected(int index) {
		tabPane.getSelectionModel().select(packetTab);
		packetData.setSelectedIndex(index);
		packetList.getSelectionModel().clearAndSelect(index);
		packetList.getFocusModel().focus(index);
		packetList.scrollTo(index);
	}
	
	@Override
	public void onObjectSelected(long objectId) {
		tabPane.getSelectionModel().select(objectTab);
		packetData.setSelectedId(objectId);
		SWGObject obj = packetData.getObject(objectId);
		if (obj == null) {
			System.err.println("Object not found for ID: " + objectId);
			return;
		}
		int index = objectItems.indexOf(obj);
		if (index == -1) {
			System.err.println("Item index not found!");
			return;
		}
		packetData.setSelectedIndex(index);
		objectList.getSelectionModel().clearAndSelect(index);
		objectList.getFocusModel().focus(index);
		objectList.scrollTo(index);
	}

	@Override
	protected void layoutChildren() {
		super.layoutChildren();
		tabPane.resize(getWidth(), getHeight());
	}
	
	public void setPacketSelectedCallback(ItemSelectedCallback callback) {
		this.selectedCallback = callback;
	}
	
	public void updatePackets() {
		packetItems = FXCollections.observableArrayList(packetData.getPackets());
		Platform.runLater(() -> packetList.setItems(packetItems));
	}
	
	public void updateObjects() {
		objectItems = FXCollections.observableArrayList(packetData.getObjects());
		Platform.runLater(() -> objectList.setItems(objectItems));
	}
	
	private static class PacketListCell extends ListCell<GUIPacket> {
		
		private static final Background SOE_PACKET = new Background(new BackgroundFill(new Color(.6, .6, .6, 1), null, null));
		private static final Background SWG_PACKET = new Background(new BackgroundFill(new Color(0, .75, 0, 1), null, null));
		
	    @Override
	    public void updateItem(GUIPacket packet, boolean empty) {
	        super.updateItem(packet, empty);
	        if (empty) {
	            setText(null);
	            setGraphic(null);
	        } else {
	            setText(packet.toString());
	            setGraphic(null);
	            if (packet.isSWGPacket())
	            	setBackground(SWG_PACKET);
	            else
	            	setBackground(SOE_PACKET);
	        }
	    }
	}
	
	private static class ObjectListCell extends ListCell<SWGObject> {
		
		private static final Background BACKGROUND = new Background(new BackgroundFill(new Color(.8, .8, .8, 1), null, null));
		
	    @Override
	    public void updateItem(SWGObject object, boolean empty) {
	        super.updateItem(object, empty);
	        if (empty) {
	            setText(null);
	            setGraphic(null);
	            setBackground(BACKGROUND);
	        } else {
	            setText(object.getClass().getSimpleName());
	            setGraphic(null);
	            setBackground(BACKGROUND);
	        }
	    }
	}
	
}
