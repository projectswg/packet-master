package com.projectswg.layout;

import java.net.URL;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

import resources.objects.SWGObject;

import com.projectswg.ItemSelectedCallback;
import com.projectswg.PacketMasterData;
import com.projectswg.packets.ObjectSearcher;
import com.projectswg.packets.PacketSearcher;
import com.projectswg.packets.Searcher;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class ToolbarPanel extends HBox implements Initializable {
	
	private final PacketMasterData packetData;
	
	private Set<ItemSelectedCallback> callbacks;
	
	@FXML private TextField searchText;
	@FXML private Button prevButton;
	@FXML private Button nextButton;
	@FXML private CheckBox searchSOE;
	@FXML private CheckBox searchSWG;
	
	public ToolbarPanel(PacketMasterData data) {
		this.packetData = data;
		callbacks = new HashSet<>();
		LayoutLoader.load("ToolbarPanel.fxml", this, this);
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		setAlignment(Pos.CENTER_LEFT);
	}
	
	public void addPacketSelectedCallback(ItemSelectedCallback callback) {
		this.callbacks.add(callback);
	}
	
	@FXML
	private void search(ActionEvent event) {
		searchNext(event);
	}
	
	@FXML
	private void searchPrev(ActionEvent event) {
		System.out.println("Search Prev: " + searchText.getText());
		Searcher searcher;
		switch (packetData.getLeftPanelContent()) {
			case PACKETS: searcher = new PacketSearcher(packetData); break;
			case OBJECTS: searcher = new ObjectSearcher(packetData); break;
			default: searcher = null; break;
		}
		int index = searcher.searchBackward(packetData.getData().getSelectedIndex(), searchText.getText(), true);
		if (index != -1) {
			if (searcher instanceof PacketSearcher) {
				for (ItemSelectedCallback callback : callbacks)
					callback.onPacketSelected(index);
			} else if (searcher instanceof ObjectSearcher) {
				SWGObject selected = ((ObjectSearcher) searcher).getLastMatch();
				for (ItemSelectedCallback callback : callbacks)
					callback.onObjectSelected(selected.getObjectId());
			}
		} else {
			System.err.println("No packet found for search: " + searchText.getText());
		}
	}
	
	@FXML
	private void searchNext(ActionEvent event) {
		System.out.println("Search Next: " + searchText.getText());
		Searcher searcher;
		switch (packetData.getLeftPanelContent()) {
			case PACKETS: searcher = new PacketSearcher(packetData); break;
			case OBJECTS: searcher = new ObjectSearcher(packetData); break;
			default: searcher = null; break;
		}
		int index = searcher.searchForward(packetData.getData().getSelectedIndex()+1, searchText.getText(), true);
		if (index != -1) {
			switch (packetData.getLeftPanelContent()) {
				case PACKETS:
					for (ItemSelectedCallback callback : callbacks)
						callback.onPacketSelected(index);
					break;
				case OBJECTS: {
					SWGObject selected = packetData.getData().getObjects().get(index);
					for (ItemSelectedCallback callback : callbacks)
						callback.onObjectSelected(selected.getObjectId());
					break;
				}
			}
		} else {
			System.err.println("No packet found for search: " + searchText.getText());
		}
	}
	
}
