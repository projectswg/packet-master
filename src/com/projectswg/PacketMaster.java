package com.projectswg;

import java.io.File;
import java.lang.reflect.Field;

import com.projectswg.layout.AnalysisPanel;
import com.projectswg.layout.LayoutLoader;
import com.projectswg.layout.PacketPanel;
import com.projectswg.layout.ToolbarPanel;
import com.projectswg.packets.PacketInterface;
import com.projectswg.packets.PacketInterface.InterfacePacket;
import com.projectswg.packets.PacketInterface.PacketInterfaceCallback;
import com.projectswg.packets.PacketProcessor;
import com.projectswg.packets.PacketProcessor.CrcChangedException;

import javafx.application.Application;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.FileChooser.ExtensionFilter;

public class PacketMaster extends Application implements PacketInterfaceCallback {
	
	private Stage stage;
	
	private PacketMasterData packetMasterData;
	private PacketInterface packetInterface;
	private PacketProcessor packetProcessor;
	
	private ToolbarPanel toolbarPanel;
	private PacketPanel packetPanel;
	private AnalysisPanel analysisPanel;
	
	private GridPane root;
	
	public static final void main(String [] args) throws Exception {
		// Forces loading of local libraries
		System.setProperty("java.library.path", new File("").getAbsolutePath());
		Field fieldSysPath = ClassLoader.class.getDeclaredField("sys_paths");
		fieldSysPath.setAccessible(true);
		fieldSysPath.set(null, null);
		System.loadLibrary("jnetpcap");
		launch(args);
	}
	
	@Override
	public void init() throws Exception {
		super.init();
		packetMasterData = new PacketMasterData();
		packetInterface = new PacketInterface();
		packetProcessor = new PacketProcessor(packetMasterData);
		
		packetInterface.setCallback(this);
		toolbarPanel = new ToolbarPanel(packetMasterData);
		packetPanel = new PacketPanel(packetMasterData);
		analysisPanel = new AnalysisPanel(packetMasterData);
		packetPanel.setPacketSelectedCallback(analysisPanel);
		toolbarPanel.addPacketSelectedCallback(analysisPanel);
		toolbarPanel.addPacketSelectedCallback(packetPanel);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		this.stage = primaryStage;
		root = new GridPane();
		root.add(createMenuBar(), 0, 0, 2, 1);
		root.add(toolbarPanel, 0, 1, 2, 1);
		root.add(packetPanel, 0, 2, 1, 1);
		root.add(analysisPanel, 1, 2, 1, 1);
		setupColumnConstraints();
		setupRowConstraints();
		Scene scene = new Scene(root, 800, 640);
		scene.setRoot(root);
		scene.getStylesheets().add(LayoutLoader.getResource("src/com/projectswg/application.css").toExternalForm());
		primaryStage.setTitle("Packet Master");
		primaryStage.setScene(scene);
		primaryStage.setMinWidth(500);
		primaryStage.setMinHeight(300);
		primaryStage.show();
	}
	
	private MenuBar createMenuBar() {
		MenuBar menuBar = new MenuBar();
		Menu menuFile = new Menu("File");
		MenuItem menuFileOpen = new MenuItem("Open");
		MenuItem menuFileSave = new MenuItem("Save");
		menuFileOpen.setAccelerator(KeyCombination.keyCombination("Ctrl+O"));
		menuFileSave.setAccelerator(KeyCombination.keyCombination("Ctrl+S"));
		final ExtensionFilter filter = new ExtensionFilter("Captures Files", "*.cap", "*.pcap");
		setupMenuOpen(menuFileOpen, filter);
		setupMenuSave(menuFileSave, filter);
		menuFile.getItems().addAll(menuFileOpen, menuFileSave);
		menuBar.getMenus().addAll(menuFile);
		return menuBar;
	}
	
	private void setupMenuOpen(MenuItem menuFileOpen, ExtensionFilter filter) {
		menuFileOpen.setOnAction((action) -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Open File");
			fileChooser.setSelectedExtensionFilter(filter);
			File file = fileChooser.showOpenDialog(stage);
			if (file != null) {
				System.out.println("Opening '"+file+"' for offline capture");
				openFile(file.getAbsolutePath());
			} else {
				System.err.println("No capture file selected for open");
			}
		});
	}
	
	private void setupMenuSave(MenuItem menuFileSave, ExtensionFilter filter) {
		menuFileSave.setOnAction((action) -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Save file");
			fileChooser.setSelectedExtensionFilter(filter);
			File file = fileChooser.showSaveDialog(stage);
			if (file != null) {
				System.out.println("Saving '"+file+"'...");
				saveFile(file.getAbsolutePath());
			} else {
				System.err.println("No capture file selected for save");
			}
		});
	}
	
	private void setupColumnConstraints() {
		ColumnConstraints c1 = new ColumnConstraints();
		ColumnConstraints c2 = new ColumnConstraints();
		// Column 1: Range of 100-300 pixels
		c1.setPrefWidth(300);
		c1.setMinWidth(100);
		c1.setMaxWidth(300);
		// Column 2: 300+ pixels, to fill rest of screen
		c2.setPrefWidth(700);
		c2.setMinWidth(300);
		c2.setHgrow(Priority.ALWAYS);
		root.getColumnConstraints().addAll(c1, c2);
	}
	
	private void setupRowConstraints() {
		RowConstraints r1 = new RowConstraints();
		RowConstraints r2 = new RowConstraints();
		RowConstraints r3 = new RowConstraints();
		// Row 1: Set to 30 pixels
		r1.setMinHeight(30);
		r1.setPrefHeight(30);
		r1.setMaxHeight(30);
		r1.setValignment(VPos.CENTER);
		r1.setVgrow(Priority.NEVER);
		// Row 2: Set to 30 pixels
		r2.setMinHeight(30);
		r2.setPrefHeight(30);
		r2.setMaxHeight(30);
		r2.setValignment(VPos.CENTER);
		r2.setVgrow(Priority.NEVER);
		// Row 3: 250+ pixels, to fill rest of screen
		r3.setMinHeight(240);
		r3.setPrefHeight(240);
		r3.setVgrow(Priority.ALWAYS);
		root.getRowConstraints().addAll(r1, r2, r3);
	}
	
	@Override
	public void onCaptureStarted() {
		packetMasterData.getData().clear();
		packetProcessor.reset();
	}
	
	@Override
	public void onPacket(InterfacePacket packet) {
		try {
			packetProcessor.onPacket(packet);
		} catch (CrcChangedException e) {
			if (!packetInterface.isLive()) {
				packetInterface.restart();
			}
		}
	}
	
	@Override
	public void onCaptureFinished() {
//		gui.repaint();
		packetProcessor.onCaptureFinished();
		packetPanel.updatePackets();
		packetPanel.updateObjects();
	}
	
	public void openFile(String file) {
		packetInterface.openFile(file);
	}
	
	public void openInterface(String inter) {
		packetInterface.openLive(inter);
	}
	
	public void saveFile(String file) {
		packetInterface.saveFile(file);
	}
	
}
