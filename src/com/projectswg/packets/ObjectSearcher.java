package com.projectswg.packets;

import java.util.List;
import java.util.Locale;

import resources.objects.SWGObject;

import com.projectswg.PacketMasterData;

public class ObjectSearcher extends Searcher {
	
	private final PacketMasterData data;
	private List<SWGObject> objects;
	private SWGObject lastMatch;
	
	public ObjectSearcher(PacketMasterData data) {
		this.data = data;
		this.objects = null;
	}
	
	public SWGObject getLastMatch() {
		return lastMatch;
	}
	
	protected int getSize() {
		return data.getData().getObjects().size();
	}
	
	protected void initializeSearch(String search) {
		objects = data.getData().getObjects();
	}
	
	protected void terminateSearch() {
		
	}
	
	protected boolean isMatch(int index, String search) {
		SWGObject obj = objects.get(index);
		String name = obj.getClass().getCanonicalName();
		boolean match = name != null && name.toLowerCase(Locale.US).contains(search.toLowerCase(Locale.US));
		if (match)
			lastMatch = obj;
		return match;
	}
	
}
