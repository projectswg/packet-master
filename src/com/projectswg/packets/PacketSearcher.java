package com.projectswg.packets;

import java.nio.charset.Charset;
import java.util.Locale;

import com.projectswg.PacketMasterData;
import com.projectswg.gui.GUIPacket;

import utilities.ByteUtilities;

public class PacketSearcher extends Searcher {
	
	private final PacketMasterData data;
	private CompiledSearch compiled;
	
	public PacketSearcher(PacketMasterData data) {
		this.data = data;
		compiled = null;
	}
	
	protected int getSize() {
		return data.getData().getPackets().size();
	}
	
	protected void initializeSearch(String search) {
		compiled = new CompiledSearch(search, data.isSearchSoe(), data.isSearchSwg());
	}
	
	protected void terminateSearch() {
		
	}
	
	protected boolean isMatch(int index, String search) {
		return compiled.matches(data.getData().getPackets().get(index));
	}
	
	private static class CompiledSearch {
		
		private static final Charset ASCII   = Charset.forName("UTF-8");
		private static final Charset UNICODE = Charset.forName("UTF-16LE");
		
		private final String search;
		private final boolean searchSoe;
		private final boolean searchSwg;
		
		private String lowercase;
		private byte [] ascii;
		private byte [] unicode;
		private byte [] byteSearch;
		
		public CompiledSearch(String search, boolean searchSoe, boolean searchSwg) {
			this.search = search;
			this.searchSoe = searchSoe;
			this.searchSwg = searchSwg;
			compile();
		}
		
		private void compile() {
			lowercase = search.toLowerCase(Locale.US);
			ascii = lowercase.getBytes(ASCII);
			unicode = lowercase.getBytes(UNICODE);
			byteSearch = ByteUtilities.getHexStringArray(lowercase.replaceAll("[^A-Fa-f0-9]", ""));
		}
		
		public boolean matches(GUIPacket packet) {
			if (!searchSoe && !packet.isSWGPacket())
				return false;
			if (!searchSwg && packet.isSWGPacket())
				return false;
			if (packet.toString().toLowerCase(Locale.US).contains(lowercase))
				return true;
			byte [] data = packet.getData().array();
			for (int i = 0; i < data.length; i++) {
				if (findArray(data, ascii, i, false))
					return true;
				if (findArray(data, unicode, i, false))
					return true;
				if (findArray(data, byteSearch, i, true))
					return true;
			}
			return false;
		}
		
		private boolean findArray(byte [] data, byte [] search, int index, boolean caseSensitive) {
			int ind = 0;
			boolean match = true;
			for (ind = 0; ind < search.length && index+ind < data.length && match; ind++) {
				if (!caseSensitive && search[ind] != Character.toLowerCase(data[index+ind]))
					match = false;
				else if (caseSensitive && search[ind] != data[index+ind])
					match = false;
			}
			if (match && ind == search.length && search.length > 1)
				return true;
			return false;
		}
		
	}
	
}
