package com.projectswg.packets;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Date;

import com.projectswg.PacketMasterData;
import com.projectswg.gui.GUIPacket;

import network.PacketType;
import network.encryption.Encryption;

import com.projectswg.network.packets.Packet;
import com.projectswg.network.packets.soe.Acknowledge;
import com.projectswg.network.packets.soe.ClientNetworkStatusUpdate;
import com.projectswg.network.packets.soe.DataChannel;
import com.projectswg.network.packets.soe.Disconnect;
import com.projectswg.network.packets.soe.Fragmented;
import com.projectswg.network.packets.soe.MultiPacket;
import com.projectswg.network.packets.soe.OutOfOrder;
import com.projectswg.network.packets.soe.ServerNetworkStatusUpdate;
import com.projectswg.network.packets.soe.SessionRequest;
import com.projectswg.network.packets.soe.SessionResponse;
import com.projectswg.packets.PacketInterface.InterfacePacket;
import com.projectswg.packets.PacketPipeProcessor.ProcessedPacket;

import network.packets.swg.SWGPacket;
import network.packets.swg.holo.HoloConnectionStarted;
import network.packets.swg.zone.object_controller.ObjectController;

public class PacketProcessor {
	
	private static final int REMOTE_PING_PORT = 44462;
	
	private final PacketData packetData;
	private final PacketPipeProcessor clientProcessor;
	private final PacketPipeProcessor serverProcessor;
	private int crc;
	private boolean encrypted;
	
	public PacketProcessor(PacketMasterData packetData) {
		this.packetData = packetData.getData();
		clientProcessor = new PacketPipeProcessor();
		serverProcessor = new PacketPipeProcessor();
		encrypted = true;
	}
	
	public void reset() {
		clientProcessor.reset();
		serverProcessor.reset();
	}
	
	public void onCaptureFinished() {
		
	}
	
	public boolean onPacket(InterfacePacket packet) throws CrcChangedException {
		switch (packet.getType()) {
			case LIVE:
			case FILE_PCAP:
				return processSonyCapture(packet);
			case FILE_HCAP:
				return processHcap(packet);
			default:
				return false;
		}
	}
	
	private boolean processHcap(InterfacePacket packet) {
		ByteBuffer data = ByteBuffer.wrap(packet.getData());
		data.position(2);
		int swgOpcode = Packet.getInt(data);
		data.position(0);
		process(packet.getSource(), packet.getTime(), swgOpcode, data);
		return true;
	}
	
	private boolean processSonyCapture(InterfacePacket packet) throws CrcChangedException {
		byte [] data = packet.getData();
		int length = data.length;
		int port = packet.getSource().getPort();
		Date time = packet.getTime();
		InetSocketAddress socket = packet.getSource();
		if (length < 2)
			return false;
		if (port != REMOTE_PING_PORT) {
			if (data[0] == 0 && data[1] == 1) {
				SessionRequest request = new SessionRequest(ByteBuffer.wrap(data));
				packetData.addPacket(new GUIPacket(request, time, 1, port, true));
				reset();
				packetData.setServer(null);
				return true;
			} else if (data[0] == 0 && data[1] == 2) { // SessionResponse
				SessionResponse response = new SessionResponse(ByteBuffer.wrap(data));
				encrypted = (response.getEncryptionFlag() != 0);
				packetData.addPacket(new GUIPacket(response, time, 2, port, false));
				crc = response.getCrcSeed();
				reset();
				packetData.setServer(socket);
				return true;
			} else {
				if (data.length == 9) {
					int crc = ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN).getInt(1)^0x6CF9AF00; // AF F9 6C A1
					packetData.setServer(socket);
					if (this.crc != crc) {
						this.crc = crc;
						throw new CrcChangedException("CRC changed!");
					}
				}
				ByteBuffer decrypt = encrypted ? ByteBuffer.wrap(Encryption.decode(data, crc)) : ByteBuffer.wrap(data);
				if (decrypt.array().length >= 2) {
					process(socket, decrypt, time);
					return true;
				}
			}
		}
		return false;
	}
	
	private void process(InetSocketAddress socket, ByteBuffer data, Date time) {
		if (data.limit() < 2)
			return;
		data.position(0);
		int opcode = data.order(ByteOrder.BIG_ENDIAN).getShort() & 0xFFFF;
		data.position(data.position()-2);
		Packet packet = createPacket(data);
		if (packet != null) {
			packetData.addPacket(new GUIPacket(packet, time, opcode, socket.getPort(), !packetData.isServer(socket)));
			packet.setAddress(socket.getAddress());
			packet.setPort(socket.getPort());
			packet.setTime(time);
		}
		switch (opcode) {
			case 0x03: {
				for (byte [] pData: ((MultiPacket) packet).getPackets()) {
					process(socket, ByteBuffer.wrap(pData), time);
				}
				break;
			}
			case 0x08:
				packetData.setServer(socket);
				break;
			case 0x15:
				if (packetData.isServer(socket)) {
					clientProcessor.setMinSequence(((Acknowledge) packet).getSequence());
					ingest(clientProcessor);
				} else {
					serverProcessor.setMinSequence(((Acknowledge) packet).getSequence());
					ingest(serverProcessor);
				}
				break;
			default:
				if ((opcode < 0 || opcode >= 0x1E) && data.limit() >= 6) {
					data.position(2);
					int swgOpcode = Packet.getInt(data);
					data.position(0);
					process(socket, time, swgOpcode, data);
				}
				break;
		}
		if (packet != null) {
			if (packetData.isServer(socket)) {
				serverProcessor.feed(packet);
				ingest(serverProcessor);
			} else {
				clientProcessor.feed(packet);
				ingest(clientProcessor);
			}
		}
	}
	
	private void process(InetSocketAddress socket, Date time, int opcode, ByteBuffer data) {
		data.position(0);
		SWGPacket packet = null;
		if (opcode == ObjectController.CRC)
			packet = ObjectController.decodeController(data);
		else {
			packet = PacketType.getForCrc(opcode);
		}
		data.position(0);
		if (packet == null) {
			packet = new SWGPacket();
			packet.decode(data);
			packet.setAddress(socket.getAddress());
			packet.setPort(socket.getPort());
			packetData.addPacket(new GUIPacket(packet, PacketType.fromCrc(opcode), time, opcode, socket.getPort(), !packetData.isServer(socket)));
			return;
		}
		if (packet instanceof HoloConnectionStarted)
			packetData.setServer(socket);
		packet.decode(data);
		packet.setAddress(socket.getAddress());
		packet.setPort(socket.getPort());
		packetData.addPacket(new GUIPacket(packet, PacketType.fromCrc(opcode), time, opcode, socket.getPort(), !packetData.isServer(socket)));
	}
	
	private Packet createPacket(ByteBuffer data) {
		if (data.limit() < 2)
			return null;
		ByteOrder original = data.order();
		short opcode = data.order(ByteOrder.BIG_ENDIAN).getShort();
		data.order(original);
		switch (opcode) {
			case 0x01:	return new SessionRequest(data);
			case 0x02:	return new SessionResponse(data);
			case 0x03:	return new MultiPacket(data);
			case 0x05:	return new Disconnect(data);
			case 0x07:	return new ClientNetworkStatusUpdate(data);
			case 0x08:	return new ServerNetworkStatusUpdate(data);
			case 0x09:	return new DataChannel(data);
			case 0x0D:	return new Fragmented(data);
			case 0x11:	return new OutOfOrder(data);
			case 0x15:	return new Acknowledge(data);
			default:
				if (opcode <= 0x1E)
					System.err.println("Unknown Packet: " + opcode);
				return null;
		}
	}
	
	private void ingest(PacketPipeProcessor processor) {
		ProcessedPacket packet = processor.poll();
		while (packet != null) {
			process(new InetSocketAddress(packet.getAddress(), packet.getPort()), ByteBuffer.wrap(packet.getData()), packet.getDate());
			packet = processor.poll();
		}
	}
	
	public static class CrcChangedException extends Exception {
		
		private static final long serialVersionUID = 1L;
		
		public CrcChangedException(String message) {
			super(message);
		}
		
	}
	
}
