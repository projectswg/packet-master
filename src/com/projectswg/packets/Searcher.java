package com.projectswg.packets;

public abstract class Searcher {
	
	public int searchForward(int start, String search) {
		return searchForward(start, search, true);
	}
	
	public int searchForward(int start, String search, boolean wrap) {
		int res = search(start, getSize(), search);
		if (res != -1 || !wrap)
			return res;
		return search(0, start, search);
	}
	
	public int searchBackward(int start, String search) {
		return searchBackward(start, search, true);
	}
	
	public int searchBackward(int start, String search, boolean wrap) {
		int res = search(start, 0, search);
		if (res != -1 || !wrap)
			return res;
		return search(0, start, search);
	}
	
	/**
	 * Searches from [start, end). Which includes the starting index, but not
	 * the ending index.
	 * @param start the starting index
	 * @param end the ending index
	 * @return the index that the item was found
	 */
	public int search(int start, int end, String search) {
		return search(start, end, end > start, search);
	}
	
	private int search(int start, int end, boolean forward, String search) {
		int inc = forward ? 1 : -1;
		int index = start;
		initializeSearch(search);
		int size = getSize();
		while ((forward && start < end) || (!forward && start > end)) {
			if (index < 0 || index >= size)
				break;
			if (isMatch(index, search))
				return index;
			index += inc;
		}
		terminateSearch();
		return -1;
	}
	
	protected abstract int getSize();
	protected abstract void initializeSearch(String search);
	protected abstract void terminateSearch();
	protected abstract boolean isMatch(int index, String search);
	
}
