package com.projectswg.packets;

import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import network.packets.Packet;

import com.projectswg.network.packets.soe.DataChannel;
import com.projectswg.network.packets.soe.Fragmented;
import com.projectswg.network.packets.soe.SequencedPacket;

public class PacketPipeProcessor {
	
	private List <SequencedPacket> sequenced;
	private Queue <ProcessedPacket> processed;
	private short sequence;
	
	public PacketPipeProcessor() {
		sequenced = new ArrayList<>();
		processed = new LinkedList<>();
		sequence = -1;
	}
	
	public void reset() {
		System.out.println("RESET");
		sequence = -1;
		synchronized (sequenced) {
			sequenced.clear();
		}
		synchronized (processed) {
			processed.clear();
		}
	}
	
	public void setMinSequence(short sequence) {
		if (this.sequence < 0) {
			this.sequence = sequence;
			System.out.println("Min Seq: " + sequence);
			processSequenced();
		} else {
//			System.out.println("*");
//			while (!sequenced.isEmpty() && sequenced.get(0).getSequence() < sequence) {
//				this.sequence = (short) (sequenced.get(0).getSequence() - 1);
//				System.out.println(sequenced.get(0).getSequence() + " / " + sequence + "-"+this.sequence+" [" + sequenced.size() + "]");
//				processSequenced();
//			}
//			this.sequence = sequence;
//			processSequenced();
		}
	}
	
	public ProcessedPacket poll() {
		synchronized (processed) {
			return processed.poll();
		}
	}
	
	public void feed(Packet p) {
		if (p instanceof SequencedPacket) {
			handleSequencedPacket((SequencedPacket) p);
		}
	}
	
	private void handleSequencedPacket(SequencedPacket p) {
		synchronized (sequenced) {
			sequenced.add(p);
			Collections.sort(sequenced);
			processSequenced();
		}
	}
	
	private void processSequenced() {
		short prevSequence = sequence;
		do {
			prevSequence = sequence;
			processDataChannel();
			processFragmented();
		} while (sequence != prevSequence);
	}
	
	private void processDataChannel() {
		while (!sequenced.isEmpty()) {
			SequencedPacket sp = sequenced.get(0);
			int comp = Short.compare(sp.getSequence(), (short)(sequence+1));
			if (comp > 0 || !(sp instanceof DataChannel)) {
				System.out.println("> " + sp.getSequence() + " " + (sequence+1));
				break;
			}
			sequenced.remove(0);
			if (comp < 0) {
				System.out.println("< " + sp.getSequence() + " " + (sequence+1));
				continue;
			}
			sequence++;
			System.out.println("PRC " + sp.getSequence());
			processDataChannel((DataChannel) sp);
		}
	}
	
	private void processFragmented() {
		while (!sequenced.isEmpty()) {
			if (!isFragmentedReady())
				break;
			SequencedPacket sp = sequenced.get(0);
			int comp = Short.compare(sp.getSequence(), (short)(sequence+1));
			if (comp > 0 || !(sp instanceof Fragmented))
				break;
			sequenced.remove(0);
			if (comp < 0)
				continue;
			sequence++;
			Fragmented f = (Fragmented) sp;
			ByteBuffer combined = ByteBuffer.allocate(getFragmentedSize(f));
			f.getPacketData().position(8);
			combined.put(f.getPacketData());
			processRemainingFragmented(combined);
			synchronized (processed) {
				processed.add(new ProcessedPacket(f.getAddress(), f.getPort(), f.getTime(), combined.array()));
			}
		}
	}
	
	private void processRemainingFragmented(ByteBuffer combined) {
		while (combined.hasRemaining()) {
			SequencedPacket sp = sequenced.get(0);
			int comp = Short.compare(sp.getSequence(), (short)(sequence+1));
			if (comp > 0 || !(sp instanceof Fragmented))
				break;
			sequenced.remove(0);
			if (comp < 0)
				continue;
			sequence++;
			Fragmented f = (Fragmented) sp;
			f.getPacketData().position(4);
			combined.put(f.getPacketData());
		}
	}
	
	private void processDataChannel(DataChannel data) {
		synchronized (processed) {
			for (byte [] inner : data.getPackets()) {
				processed.add(new ProcessedPacket(data.getAddress(), data.getPort(), data.getTime(), inner));
			}
		}
	}
	
	private boolean isFragmentedReady() {
		short seq = sequence;
		int size = 0, index = 0;
		for (SequencedPacket sp : sequenced) {
			int comp = Short.compare(sp.getSequence(), (short) (seq+1));
			if (comp < 0)
				continue;
			if (comp > 0 || !(sp instanceof Fragmented))
				return false;
			seq++;
			Fragmented f = (Fragmented) sp;
			if (index == 0) {
				ByteBuffer data = f.getPacketData();
				data.position(4);
				size = Packet.getNetInt(data);
				index = data.remaining();
			} else {
				index += f.getPacketData().limit()-4;
				if (index >= size) {
					return index == size;
				}
			}
		}
		return false;
	}
	
	private int getFragmentedSize(Fragmented f) {
		ByteBuffer data = f.getPacketData();
		data.position(4);
		return Packet.getNetInt(data);
	}
	
	public static class ProcessedPacket {
		
		private final InetAddress addr;
		private final int port;
		private final Date date;
		private final byte [] data;
		
		private ProcessedPacket(InetAddress addr, int port, Date date, byte [] data) {
			this.addr = addr;
			this.port = port;
			this.date = date;
			this.data = data;
		}
		
		public InetAddress getAddress() { return addr; }
		public int getPort() { return port; }
		public Date getDate() { return date; }
		public byte[] getData() { return data; }
		
	}
	
}
