package com.projectswg.packets;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayDeque;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.jnetpcap.Pcap;
import org.jnetpcap.PcapBpfProgram;
import org.jnetpcap.nio.JBuffer;
import org.jnetpcap.packet.Payload;
import org.jnetpcap.packet.PcapPacket;
import org.jnetpcap.packet.PcapPacketHandler;
import org.jnetpcap.protocol.lan.Ethernet;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Udp;

import utilities.ThreadUtilities;

public class PacketInterface {
	
	private final Queue<InterfacePacket> packetQueue;
	private PacketInterfaceCallback callback;
	private PacketAnalysisThread analysis;
	private boolean isLive;
	
	public PacketInterface() {
		packetQueue = new ArrayDeque<>();
		analysis = null;
		callback = null;
	}
	
	public void setCallback(PacketInterfaceCallback callback) {
		this.callback = callback;
	}
	
	public boolean isLive() {
		return isLive;
	}
	
	public void openLive(String inter) {
		stop();
		synchronized (packetQueue) {
			packetQueue.clear();
		}
		analysis = new LivePacketAnalysis(inter);
		isLive = true;
		analysis.start();
	}
	
	public void openFile(String file) {
		stop();
		synchronized (packetQueue) {
			packetQueue.clear();
		}
		analysis = new FilePacketAnalysis(file);
		isLive = false;
		analysis.start();
	}
	
	public void stop() {
		if (analysis != null) {
			System.out.println("Stopping old analysis! Type: " + analysis);
			analysis.stop();
			analysis.awaitTermination();
			analysis = null;
		}
	}
	
	public void restart() {
		if (isLive())
			throw new IllegalStateException("Cannot restart when live capturing");
		if (analysis == null)
			throw new IllegalStateException("Cannot restart when wasn't started in the first place");
		if (!(analysis instanceof FilePacketAnalysis))
			throw new IllegalStateException("Internal error... not sure how this happens");
		openFile(((FilePacketAnalysis) analysis).getFile());
	}
	
	public void saveFile(String file) {
		
	}
	
	public interface PacketInterfaceCallback {
		void onCaptureStarted();
		void onPacket(InterfacePacket packet);
		void onCaptureFinished();
	}
	
	private class LivePacketAnalysis extends PacketAnalysisThread {
		
		private String inter;
		
		public LivePacketAnalysis(String inter) {
			this.inter = inter;
		}
		
		public void run() {
			final StringBuilder errbuf = new StringBuilder(); // For any error msgs
			System.out.println("Opening "+inter+" for live capture");
			Pcap pcap = Pcap.openLive(inter, 1024, Pcap.MODE_PROMISCUOUS, 60000, errbuf);
			try {
				if (pcap == null) {
					System.err.println("Error while opening device for capture: " + errbuf.toString());
					System.err.println("Interface: " + inter);
					return;
				}
				runAnalysis(pcap, true);
			} finally {
				pcap.close();
				System.out.println("Finished live capture.");
			}
		}
		
		public String toString() {
			return "LivePacketAnalysis['"+inter+"']";
		}
		
	}
	
	private class FilePacketAnalysis extends PacketAnalysisThread {
		
		private String file;
		
		public FilePacketAnalysis(String file) {
			this.file = file;
		}
		
		public String getFile() {
			return file;
		}
		
		public void run() {
			String extension = file.substring(file.lastIndexOf('.')+1);
			switch (extension) {
				case "pcap":
				case "cap":
					runPcap();
					break;
				case "hcap":
					runHcap();
					break;
			}
		}
		
		private void runPcap() {
			final StringBuilder errbuf = new StringBuilder(); // For any error msgs
			Pcap pcap = Pcap.openOffline(file, errbuf);
			try {
				if (pcap == null) {
					System.err.println("Error while opening file for capture: " + errbuf.toString());
					System.err.println("File: " + file);
					return;
				}
				runAnalysis(pcap, false);
			} finally {
				pcap.close();
				System.out.println("Finished offline capture.");
			}
		}
		
		private void runHcap() {
			ExecutorService callbackExecutor = Executors.newSingleThreadExecutor(ThreadUtilities.newThreadFactory(toString()+"-CallbackExecutor"));
			try (DataInputStream dataIn = new DataInputStream(new FileInputStream(new File(file)))) {
				initAnalysis(callbackExecutor);
				int version = dataIn.readByte();
				switch (version) {
					case 1:
						runHcap1(callbackExecutor, dataIn);
						break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				cleanupAnalysis(callbackExecutor);
			}
		}
		
		private void runHcap1(ExecutorService callbackExecutor, DataInputStream dataIn) throws IOException {
			Map<String, String> properties = new HashMap<>();
			int count = dataIn.readByte();
			for (int i = 0; i < count; i++) {
				String [] property = dataIn.readUTF().split("=", 2);
				properties.put(property[0], property[1]);
			}
			while (dataIn.available() > 0) {
				dataIn.readByte(); // isServer
				Date time = new Date(dataIn.readLong());
				InetSocketAddress source = readSocket(dataIn);
				InetSocketAddress destination = readSocket(dataIn);
				byte [] data = new byte[dataIn.readShort() & 0xFFFF];
				dataIn.read(data);
				addPacket(new InterfacePacket(source, destination, time, data, InterfaceType.FILE_HCAP));
				processPacket(callbackExecutor);
			}
		}
		
		private InetSocketAddress readSocket(DataInputStream dataIn) throws IOException {
			int length = dataIn.readByte();
			byte [] addr = new byte[length];
			dataIn.read(addr);
			int port = dataIn.readShort() & 0xFFFF;
			return new InetSocketAddress(InetAddress.getByAddress(addr), port);
		}
		
		public String toString() {
			return "FilePacketAnalysis['"+file+"']";
		}
		
	}
	
	private abstract class PacketAnalysisThread implements Runnable {
		
		private final AtomicBoolean analysisRunning = new AtomicBoolean(false);
		private Thread thread	= null;
		private boolean running	= false;
		
		public void start() {
			if (running)
				stop();
			thread = new Thread(this);
			running = true;
			thread.start();
		}
		
		public void stop() {
			if (!running || thread == null)
				return;
			running = false;
			thread.interrupt();
		}
		
		public void awaitTermination() {
			while (analysisRunning.get()) {
				try {
					Thread.sleep(5);
				} catch (InterruptedException e) {
					break;
				}
			}
			thread = null;
		}
		
		protected void runAnalysis(Pcap pcap, boolean live) {
			ExecutorService callbackExecutor = Executors.newSingleThreadExecutor(ThreadUtilities.newThreadFactory(toString()+"-CallbackExecutor"));
			Udp udp = new Udp();
			Ip4 ip = new Ip4();
			try {
				initAnalysis(callbackExecutor);
				PcapBpfProgram program = new PcapBpfProgram();
				if (pcap.compile(program, "udp", 0, 0xFFFFFF00) != Pcap.OK) {
					System.err.println(pcap.getErr());
					return;
				}
				if (pcap.setFilter(program) != Pcap.OK) {
					System.err.println(pcap.getErr());
					return;
				}
				pcap.loop(-1, (PcapPacketHandler<String>) (packet, arg) -> {
					if (!running)
						pcap.breakloop();
					int headerRet = processHeaders(packet, udp, ip);
					if (headerRet == -1)
						pcap.breakloop();
					else if (headerRet == 0)
						process(callbackExecutor, packet, udp, ip, live ? InterfaceType.LIVE : InterfaceType.FILE_PCAP);
					else
						System.err.println("Failed to process packet with code: " + headerRet);
				}, "");
			} finally {
				cleanupAnalysis(callbackExecutor);
			}
		}
		
		protected void initAnalysis(ExecutorService callbackExecutor) {
			analysisRunning.set(true);
			if (callback != null) {
				callbackExecutor.execute(() -> {
					try {
						callback.onCaptureStarted();
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
			}
		}
		
		protected void addPacket(InterfacePacket packet) {
			synchronized (packetQueue) {
				packetQueue.add(packet);
			}
		}
		
		protected void processPacket(ExecutorService callbackExecutor) {
			callbackExecutor.execute(() -> {
				InterfacePacket raw = null;
				synchronized (packetQueue) {
					raw = packetQueue.poll();
				}
				if (raw == null) {
					System.err.println("No packet to process! Queue is empty.");
					return;
				}
				try {
					if (callback != null)
						callback.onPacket(raw);
				} catch (Exception e) {
					e.printStackTrace();
				}
			});
		}
		
		protected void cleanupAnalysis(ExecutorService callbackExecutor) {
			running = false;
			thread = null;
			if (analysis == this)
				analysis = null;
			if (callback != null)
				callbackExecutor.execute(() -> {
					try {
						callback.onCaptureFinished();
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
			callbackExecutor.shutdown();
			try {
				callbackExecutor.awaitTermination(5, TimeUnit.MINUTES);
			} catch (InterruptedException e) {
				callbackExecutor.shutdownNow();
			}
			analysisRunning.set(false);
		}
		
		private void process(ExecutorService callbackExecutor, PcapPacket packet, Udp udp, Ip4 ip, InterfaceType type) {
			try {
				process(packet, ip, udp, type);
				processPacket(callbackExecutor);
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
		}
		
		private int processHeaders(PcapPacket packet, Udp udp, Ip4 ip) {
			try {
				packet.scan(Ethernet.ID);
			} catch (Exception e) {
				e.printStackTrace();
				return -1;
			}
			if (!packet.hasHeader(ip))
				return 2;
			if (!packet.hasHeader(udp))
				return 3;
			return 0;
		}
		
		private void process(PcapPacket packet, Ip4 ip, Udp udp, InterfaceType type) throws UnknownHostException {
			Payload p = new Payload();
			InetSocketAddress source = new InetSocketAddress(InetAddress.getByAddress(ip.source()), udp.source());
			InetSocketAddress destination = new InetSocketAddress(InetAddress.getByAddress(ip.destination()), udp.destination());
			JBuffer buffer = packet.getHeader(p);
			if (buffer == null) {
				System.err.println("JBuffer is null! Dropping packet");
				return;
			}
			Date time = new Date(packet.getCaptureHeader().timestampInMillis());
			addPacket(new InterfacePacket(source, destination, time, buffer.getByteArray(0, new byte[buffer.size()]), type));
		}
		
	}
	
	public static class InterfacePacket {
		
		private final InetSocketAddress source;
		private final InetSocketAddress destination;
		private final Date time;
		private final byte [] data;
		private final InterfaceType type;
		
		public InterfacePacket(InetSocketAddress source, InetSocketAddress destination, Date time, byte [] data, InterfaceType type) {
			this.destination = destination;
			this.source = source;
			this.time = time;
			this.data = data;
			this.type = type;
		}
		
		public InetSocketAddress getSource() { return source; }
		public InetSocketAddress getDestination() { return destination; }
		public Date getTime() { return time; }
		public byte[] getData() { return data; }
		public InterfaceType getType() { return type; }
	}
	
	public enum InterfaceType {
		LIVE,
		FILE_PCAP,
		FILE_HCAP
	}
	
}
