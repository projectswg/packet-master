package com.projectswg.packets;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import network.packets.Packet;
import network.packets.swg.zone.SceneCreateObjectByCrc;
import network.packets.swg.zone.UpdateContainmentMessage;
import network.packets.swg.zone.UpdatePvpStatusMessage;
import network.packets.swg.zone.baselines.Baseline;
import network.packets.swg.zone.insertion.CmdStartScene;
import resources.objects.SWGObject;
import resources.objects.tangible.TangibleObject;
import resources.server_info.CrcDatabase;
import services.objects.ObjectCreator;

import com.projectswg.gui.GUIPacket;

public class PacketData {
	
	private static final CrcDatabase CRC_DATABASE = new CrcDatabase();
	
	private final LinkedHashMap<Long, SWGObject> objects;
	
	private List<GUIPacket> packets;
	private InetSocketAddress server;
	private int selectedIndex;
	private long selectedId;
	
	public PacketData() {
		objects = new LinkedHashMap<>();
		packets = new ArrayList<>();
		server = null;
		selectedIndex = 0;
		selectedId = 0;
	}
	
	public void addPacket(GUIPacket packet) {
		if (packet.getPacket() == null)
			throw new NullPointerException();
		synchronized (packets) {
			packets.add(packet);
		}
		processPacket(packet.getPacket());
	}
	
	public void clear() {
		synchronized (packets) {
			packets.clear();
		}
		synchronized (objects) {
			objects.clear();
		}
	}
	
	public GUIPacket get(int index) {
		synchronized (packets) {
			return packets.get(index);
		}
	}
	
	public List<GUIPacket> getPackets() {
		synchronized (packets) {
			return new ArrayList<>(packets);
		}
	}
	
	public List<SWGObject> getObjects() {
		return new ArrayList<>(objects.values());
	}
	
	public SWGObject getObject(long objectId) {
		return objects.get(objectId);
	}
	
	public int size() {
		synchronized (packets) {
			return packets.size();
		}
	}
	
	public int getSelectedIndex() {
		return selectedIndex;
	}
	
	public long getSelectedId() {
		return selectedId;
	}
	
	public InetSocketAddress getServer() {
		return server;
	}
	
	public void setServer(InetSocketAddress server) {
		this.server = server;
	}
	
	public void setSelectedIndex(int index) {
		this.selectedIndex = index;
	}
	
	public void setSelectedId(long id) {
		this.selectedId = id;
	}
	
	public boolean isServer(InetSocketAddress addr) {
		return server != null && server.equals(addr);
	}
	
	public boolean isServerDefined() {
		return server != null;
	}
	
	private void processPacket(Packet p) {
		if (p instanceof SceneCreateObjectByCrc)
			processCreateObject((SceneCreateObjectByCrc) p);
		else if (p instanceof CmdStartScene)
			processCmdStartScene((CmdStartScene) p);
		else if (p instanceof UpdateContainmentMessage)
			processUpdateContainment((UpdateContainmentMessage) p);
		else if (p instanceof UpdatePvpStatusMessage)
			processUpdatePvpStatus((UpdatePvpStatusMessage) p);
		else if (p instanceof Baseline)
			processBaseline((Baseline) p);
	}
	
	private void processCreateObject(SceneCreateObjectByCrc create) {
		String template = CRC_DATABASE.getString(create.getObjectCrc());
		SWGObject obj = null;
		if (template != null)
			obj = ObjectCreator.createObjectFromTemplate(create.getObjectId(), template);
		if (obj != null) {
			obj.setLocation(create.getLocation());
			SWGObject recreate = objects.put(obj.getObjectId(), obj);
			if (recreate != null) {
				System.err.println("Recreating " + recreate.getTemplate() + " with " + template);
			}
		} else
			System.err.println("Failed to create: " + template);
	}
	
	private void processCmdStartScene(CmdStartScene start) {
		objects.clear();
		String template = start.getRace().getFilename();
		SWGObject obj = ObjectCreator.createObjectFromTemplate(start.getCharacterId(), template);
		if (obj != null) {
			obj.setLocation(start.getLocation());
			objects.put(obj.getObjectId(), obj);
		} else
			System.err.println("Failed to create: " + template);
	}
	
	private void processUpdateContainment(UpdateContainmentMessage update) {
		SWGObject obj = objects.get(update.getObjectId());
		if (obj == null) {
			System.err.println("No such ID for update containment! ID: " + update.getObjectId());
			return;
		}
		SWGObject parent = objects.get(update.getContainerId());
		if (parent == null) {
			System.err.println("Unkown parent! ID: " + update.getObjectId() + "  Parent: " + update.getContainerId());
		}
		obj.setParent(parent);
		if (parent != null)
			parent.addObject(obj);
	}
	
	private void processUpdatePvpStatus(UpdatePvpStatusMessage update) {
		SWGObject obj = objects.get(update.getObjectId());
		if (obj == null) {
			System.err.printf("No such ID for update pvp! ID: %d  [%16X]%n", update.getObjectId(), update.getObjectId());
			return;
		}
		if (!(obj instanceof TangibleObject)) {
			System.err.println("Object is not a tangible object! Cannot set pvp flags: " + obj);
			return;
		}
		((TangibleObject) obj).setPvpFlags(update.getPvpFlags());
	}
	
	private void processBaseline(Baseline baseline) {
		SWGObject obj = objects.get(baseline.getObjectId());
		if (obj != null)
			obj.parseBaseline(baseline);
		else
			System.err.println("Baseline for object not created! ID: " + baseline.getObjectId());
	}
	
}
